﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using NETMouse;

namespace ImmutableStack
{
    /// <summary>
    /// Represents a strongly typed immutable stack.
    /// </summary>
    /// <typeparam name="T">The value type.</typeparam>
    [Serializable]
    [DebuggerDisplay("Count = {Count}")]
    [DebuggerTypeProxy(typeof(NETMouse.Debug.Generic.CollectionDebugView<>))]
    public class ImmutableStack<T> : ICollection<T>,
        IReadOnlyCollection<T>,
        ICollection
    {
        /// <summary>
        /// The empty stack.
        /// </summary>
        public static ImmutableStack<T> Empty = new ImmutableStack<T>();

        /// <summary>
        /// The elements count.
        /// </summary>
        public int Count => isEmpty ? 0 : (1 + (tail?.Count ?? 0));

        /// <summary>
        /// Determines whether the current collection is a synchronized one.
        /// </summary>
        public bool IsSynchronized => false;
        
        /// <summary>
        /// The synchronization object.
        /// </summary>
        [field: NonSerialized]
        public object SyncRoot { get; } = new object();

        /// <summary>
        /// Determines whether the current collection is a readonly one.
        /// </summary>
        public bool IsReadOnly => false;

        /// <summary>
        /// Constructs the new instance of ImmutableStack from it's head and tail.
        /// </summary>
        /// <param name="head">The head.</param>
        /// <param name="tail">The tail.</param>
        public ImmutableStack(T head, ImmutableStack<T> tail = null)
        {
            this.head = head;
            this.tail = tail;
        }
        
        /// <summary>
        /// Pushes the particular item into the current stack.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns>The new stack.</returns>
        public ImmutableStack<T> Push(T item)
        {
            return new ImmutableStack<T>(item, this);
        }
        
        /// <summary>
        /// Retrieves the topmost item from the current stack.
        /// </summary>
        /// <returns>The new stack.</returns>
        public ImmutableStack<T> Pop()
        {
            if (isEmpty)
                throw new InvalidOperationException(nameof(Count));
            
            if (tail.Count > 0)
                return new ImmutableStack<T>(tail.head, tail.tail);
            return Empty;
        }

        /// <summary>
        /// Obtains the topmost item of the current stack.
        /// </summary>
        /// <returns>The topmost item.</returns>
        public T Peek()
        {
            if (isEmpty)
                throw new InvalidOperationException(nameof(Count));
            
            return head;
        }
        
        /// <summary>
        /// Clears the current collection.
        /// </summary>
        /// <returns>The new collection.</returns>
        public ImmutableStack<T> Clear()
        {
            if (isEmpty)
                throw new InvalidOperationException(nameof(Count));
            
            return Empty;
        }

        /// <summary>
        /// Evaluates whether the particular item presents in the current collection.
        /// </summary>
        /// <param name="item">Item to find.</param>
        /// <returns>true, if item presents in the current collection, false otherwise.</returns>
        public bool Contains(T item)
        {
            if (isEmpty)
                return false;
            return item.Equals(head) || tail.Contains(item);
        }

        /// <summary>
        /// Copies the current collection into the specified array from the particular index.
        /// </summary>
        /// <param name="array">The array.</param>
        /// <param name="arrayIndex">The index.</param>
        public void CopyTo(T[] array, int arrayIndex)
        {
            CheckArrayCorrectness(array, arrayIndex);
            
            ImmutableStack<T> current = this;
            for (int i = 0; i < Count; i++)
            {
                array[i + arrayIndex] = current.head;
                current = tail;
            }
        }

        /// <summary>
        /// Copies the current collection into the specified array from the particular index.
        /// </summary>
        /// <param name="array">The array.</param>
        /// <param name="index">The index.</param>
        void ICollection.CopyTo(Array array, int index)
        {
            CheckArrayCorrectness(array, index);
            if (array.Rank != 1)
                throw new RankException(nameof(array));
            if (array.GetLowerBound(0) != 0)
                throw new ArgumentException(nameof(array));
            
            ImmutableStack<T> current = this;
            for (int i = 0; i < Count; i++)
            {
                array.SetValue(current.head, i + index);
                current = tail;
            }
        }
        
        void ICollection<T>.Add(T item)
        {
            Push(item);
        }

        bool ICollection<T>.Remove(T item)
        {
            throw new NotSupportedException();
        }
        
        void ICollection<T>.Clear()
        {
            throw new NotSupportedException();
        }

        /// <summary>
        /// Returns the enumerator of the current collection.
        /// </summary>
        /// <returns>The enumerator.</returns>
        public Enumerator GetEnumerator()
        {
            return new Enumerator(this);
        }
        
        /// <summary>
        /// Returns the enumerator of the current collection.
        /// </summary>
        /// <returns>The enumerator.</returns>
        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            return GetEnumerator();
        }
        
        /// <summary>
        /// Returns the enumerator of the current collection.
        /// </summary>
        /// <returns>The enumerator.</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
        
        private ImmutableStack()
        {
            isEmpty = true;
        }

        private bool isEmpty;
        private readonly T head;
        private readonly ImmutableStack<T> tail;

        private void CheckArrayCorrectness(Array array, int index)
        {
            if (array is null)
                throw new ArgumentNullException();
            if (!index.IsBetween(0, array.Length - 1))
                throw new ArgumentOutOfRangeException(nameof(index));
            if (array.Length - index < Count)
                throw new ArgumentException(nameof(array));
        }
        
        /// <summary>
        /// The enumerator.
        /// </summary>
        public struct Enumerator : IEnumerator<T>
        {
            /// <summary>
            /// The current element.
            /// </summary>
            public T Current
            {
                get
                {
                    if (!enumerationStarted)
                        throw new InvalidOperationException();

                    return current.head;
                }
            }

            /// <summary>
            /// The current element.
            /// </summary>
            object IEnumerator.Current => Current;
            
            /// <summary>
            /// Constructs the new instance of Enumerator from the particular stack.
            /// </summary>
            /// <param name="source">The stack.</param>
            public Enumerator(ImmutableStack<T> source) : this()
            {
                this.source = source;
            }
            
            /// <summary>
            /// Moves the current enumerator on one unit and returns the value that indicates whether the futher traversing is possible.
            /// </summary>
            /// <returns>The value that indicates whether the futher traversing is possible.</returns>
            public bool MoveNext()
            {
                if (!enumerationStarted)
                {
                    current = source;
                    enumerationStarted = true;
                }
                else
                    current = current.tail;

                return current != null;
            }

            /// <summary>
            /// Resets the current enumerator.
            /// </summary>
            public void Reset()
            {
                enumerationStarted = false;
                current = null;
            }
            
            /// <summary>
            /// Releases all unmanaged resources those were used in the current enumerator.
            /// </summary>
            public void Dispose()
            {
            }
            
            private readonly ImmutableStack<T> source;
            private bool enumerationStarted;
            private ImmutableStack<T> current;
        }
    }
}
